# Progress Reporting

## Overview

* Request from power-ups to provide weekly/monthly report to reflect on overall status of an initiative
* Organization is traditionally processes-heavy and is in a regulated industry
* Organization is using or transforming to Agile methodology
* Some teams have transitioned to sprints methodology while most of the organization has not
* A progress status report to reflect an overall health of the initiatives at the macro level has been requested
* Individual project Jira boards are not as helpful as they are focused at a sprint level rather than at the initiative level
* Providing a status report using a traditional [PMO](https://en.wikipedia.org/wiki/Project_management_office) approach would require a considerable on-going and a duplicative effort

## Requirements for the progress report 

1. Business objectives and critical success factors
2. Customer value and sponsors
3. Timeline and key milestones
4. Resources
5. Budget
6. Dependencies and partnerships
7. Risks and mitigations
8. Stakeholders and governance structure

## Original Plan

* Create a Confluence page with a weekly/monthly status report reflecting the status
* Proven to be a tad time-consuming to agree on a format alone, never mind the content
* Predicted to be a time-consuming exercise updating on a periodic basis and copying data from Jira tickets

## Competing Idea

1. Let every team to be productive by managing their own Jira boards and task lists
2. Impose common data attributes for a collective porgress reporting, e.g. business objectives and internal/external dependencies
3. Ingest data using Jira restful api's into Elastic Search
4. Leverage Elastic Search and Kibana to create a dashboard 
    * Aggregate common data elements from the project's Jira
    * Use [tag cloud](https://www.elastic.co/blog/tag-that-cloud-a-new-visualization-in-kibana) for e.g. most common business objectives listed in Jira tickets
    * Leverage Kibana vertical bar chart to visually represent import indicators, e.g. number of issues completed per week
    * Use Kibana list with filter and a [hyper link](.https://github.com/elastic/kibana/issues/363) to a specific Jira issue details leveraging [scripted fields](.https://www.elastic.co/blog/using-painless-kibana-scripted-fields)
    * Leverage [Lucene](https://lucene.apache.org/core/2_9_4/queryparsersyntax.html) filtering capabilities and [Kibana time filter](https://www.elastic.co/guide/en/kibana/current/set-time-filter.html) to narrow-down dashboard scope

## Conceptual Solution

![conceptual solution diagram](./media/conceptual.jpg)

## Hackathon Plan

* Create a cloud Jira account for prototype purposes
* Review customization options available in Jira e.g. for adding `business objectives` and other tags
* Spin-off new a Elastic Search cloud cluster
* Manually create documents to represent a Jira issue data set
* Review Elastic Search [mapping](https://www.elastic.co/guide/en/elasticsearch/reference/current/mapping.html) to ensure accuracy of field data types, e.g. a dependency should be treated as a [keyword](https://www.elastic.co/guide/en/elasticsearch/reference/current/keyword.html) rather than a collection of tokens

## PoC Solution

![poc solution diagram](./media/poc.jpg)

## Implementation Lessons

* Jira allows adding custom fields to issues for manual entry of business objectives, success factors, and etc.
* Custom field data types supported:
  * Check-box list: select from the values configured by Jira administrator, most convenient for cross-projects analysis, no ad-hoc item entry is available
  * Text area: free style date entry, not as easy to establish common set of values, ElasticSearch will analyze the entire field content, maybe less that useful for large number of issues analysis
  * Labels: multi-value data entry with option to select a previously or a newly entered value, does not support spaces (can be replaced with dashes instead), less standard than check-box list, but allows ad-hoc data entry
  * Multi-user select: helpful in selecting people, e.g. for `Sponosrs` fields, people available for selection must be Jira users
  ![jira custom field](./media/jira-custom-field.png)
* Jira supports [webhooks](https://developer.atlassian.com/cloud/jira/platform/webhooks/) configured by Jira administrator to post data on issues [CRUD](https://en.wikipedia.org/wiki/Create,_read,_update_and_delete) operations to an arbitrary url
* Webhook functionality can be leveraged to trigger an end-point to update Elastic Search
* Jira comes with restful API to query individual issues and other entities
* Elastic Search aggregation for most common words: [Significant Terms Aggregation](https://www.elastic.co/guide/en/elasticsearch/reference/current/search-aggregations-bucket-significantterms-aggregation.html) did not produce results, maybe a more significant data set is required
* Elastic Search [Terms Aggregation](https://www.elastic.co/guide/en/elasticsearch/reference/current/search-aggregations-bucket-terms-aggregation.html) supports [keyword](http://www.elastic.co/guide/en/elasticsearch/reference/current/keyword.html) only
* Kibana script field functionality allows adding a direct link to Jira Issue from search results. Make sure to use url template: `https://vk-bank.atlassian.net/browse/{{value}}` for formatting and `keyword` in painless script on text fields: `return doc['key.keyword'].value;`
* Kibana [Tag Cloud](https://www.elastic.co/guide/en/kibana/current/tagcloud-chart.html) and [Table](https://www.elastic.co/guide/en/kibana/current/data-table.html) seems to be most useful for top-N values visualization
* Kibana charts could be used for other quantitative analysis, e.g. effort spent, total points, how long it takes to close a ticket, etc.
* [AWS API-Gateway](https://aws.amazon.com/api-gateway/) & [AWS Lambda](https://aws.amazon.com/lambda/) were used to respond to webhook request:
  * fetch full issue using [Jira Restful API](https://developer.atlassian.com/cloud/jira/platform/rest/)
  * transform Jira issue json to replace `customfield_10001` with `BusinessObjectives`
  * index data to Elastic Search using https://cloud.elastic.co/
* Kibana dashboard has been used to aggregate reporting fields e.g. Business Objectives:
  ![kibana dashboard](./media/kibana-dashboard.png)
* Kibana scripted field has been used to provide a link to issue details:
  ![kibana search](./media/kibana-search.png)
* Kiabana built-in capabilities to filter e.g. by project and by time-frame; and to set the dashboard to auto-refresh
  ![kibana filter](./media/kibana-filter.png)

## Technical Points

* Scripted field in Kibana was a bit of a trouble because of the `.keyword` on a text field, e.g.: `return doc['key.keyword'].value;` instead of `return doc['key'].value;`
* To use `Terms` aggregation on a free-style text, field-data needs to be enabled through index mapping:
```
{
  "jiraissues": {
    "mappings": {
      "_doc": {
        "Risks": {
          "full_name": "Risks",
          "mapping": {
            "Risks": {
              "type": "text",
              "fields": {
                "keyword": {
                  "type": "keyword",
                  "ignore_above": 256
                }
              },
              "fielddata": true
            }
          }
        }
      }
    }
  }
}
```
